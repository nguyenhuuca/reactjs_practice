import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./css/bootstrap.css";

function Square(props) {
  return (
    <button
      className={"square " + (props.value === "X" ? "blue" : "red")}
      onClick={props.onClick}
    >
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,
      firstPlayer: "Tom",
      secondPlayer: "Jerry"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([
        {
          squares: squares
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      firstPlayer: this.state.firstPlayer,
      secondPlayer: this.state.secondPlayer
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);
    const persion = { X: this.state.firstPlayer, O: this.state.secondPlayer };

    const moves = history.map((step, move) => {
      const desc = move ? "Go to move #" + move : "Go to game start";
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    let result;
    if (winner) {
      status = "Winner: " + persion[winner];
      result = persion[winner] + " win!!";
    } else {
      const isDraw = checkDraw(current.squares);
      if (isDraw) {
        status = "draw";
        result = "draw";
      } else {
        status =
          "Next player: " +
          (this.state.xIsNext
            ? this.state.firstPlayer
            : this.state.secondPlayer);
      }
    }

    return (
      <div className="container monster-container">
        <div className="row game-top">
          <div className="game monster-col">
            <div className="monster-col">
              First Player:
              <input
                type="text"
                name="firstPlayer"
                value={this.state.firstPlayer}
                onChange={this.handleChange}
              />
            </div>
            <div className="monster-col">
              Second Player:
              <input
                type="text"
                name="secondPlayer"
                value={this.state.secondPlayer}
                onChange={this.handleChange}
              />
            </div>
            <div className="game-board">
              <Board
                squares={current.squares}
                onClick={i => this.handleClick(i)}
              />
            </div>
            <div className="game-info">
              <div>{status}</div>
              <ol>{moves}</ol>
            </div>
          </div>
        </div>
        <div className="row game-bottom" />
        <div className="row fixed-info">
          <div className="col-md-6">
            First Player: {this.state.firstPlayer} - Second Player:{
              this.state.secondPlayer
            }
          </div>
          <div className="col-md-6 pull-right">Result:{result}</div>
        </div>
      </div>
    );
  }
}

//=============================
ReactDOM.render(<Game />, document.getElementById("root"));

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

function checkDraw(squares) {
  for (let i = 0; i < squares.length; i++) {
    if (!squares[i]) return false;
  }
  return true;
}
